## Removeing system-d Resolve

This guide is about replaceing `systemd-resolved` with `dnsmasq` on ubuntu 18.04 from [this post.](https://askubuntu.com/questions/898605/how-to-disable-systemd-resolved-and-resolve-dns-with-dnsmasq) 



Before removeing systemd-resolve remember to get dnsmasq 

*All of these are using root*

```
apt install dnsmasq
```

Then create a file:
```
cat /etc/systemd/resolved.conf.d/noresolved.conf 

[Resolve]
DNSStubListener=no
```
Removes Systemd-resolve's listner on port 53

Then disable resolve

```
systemctl disable systemd-resolved.service
```

```
rm /etc/resolv.conf
touch /etc/resolv.conf
```

```
cat /etc/NetworkManager/conf.d/disableresolv.conf 
[main]
dns=dnsmasq
```

```
sudo rm /etc/resolv.conf; sudo ln -s /var/run/NetworkManager/resolv.conf /etc/resolv.conf
```

```
sudo systemctl restart NetworkManager
```

```
sudo systemctl stop dnsmasq
sudo systemctl start dnsmasq
```
